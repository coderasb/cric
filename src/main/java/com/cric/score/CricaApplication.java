package com.cric.score;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CricaApplication {

	public static void main(String[] args) {
		SpringApplication.run(CricaApplication.class, args);
	}

}
