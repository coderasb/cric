package com.cric.score.controller;

import com.cric.score.domain.Score;
import com.cric.score.service.ScoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CricaController {

    @Autowired
    private ScoreService scoreService;

	@GetMapping("/")
	public String mainPage() {
		return "Welcome to Crica";
	}

	@GetMapping("/score")
	public Score getScore() {
		return scoreService.getScore();
	}

}
