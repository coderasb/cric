package com.cric.score.controller;

import com.cric.score.domain.Score;
import com.cric.score.service.ScoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ScoreController {

    @Autowired
    private ScoreService scoreService;

	@PostMapping("/update")
	//{"runs":11,"wickets":2,"overs":12.1}
	public String mainPage(@RequestBody Score score) {
		if(scoreService.setScore(score))
		    return "score has been updated successfully";
		return "";
	}

}
