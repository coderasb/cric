package com.cric.score.domain;


public class Score {
    Integer runs;
    Integer wickets;
    Double overs;

    public Integer getRuns() {
        return runs;
    }

    public void setRuns(Integer runs) {
        this.runs = runs;
    }

    public Integer getWickets() {
        return wickets;
    }

    public void setWickets(Integer wickets) {
        this.wickets = wickets;
    }

    public Double getOvers() {
        return overs;
    }

    public void setOvers(Double overs) {
        this.overs = overs;
    }

    @Override
    public String toString() {
        return "ScoreRequest{" +
                "runs=" + runs +
                ", wickets=" + wickets +
                ", overs=" + overs +
                '}';
    }
}
