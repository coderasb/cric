package com.cric.score.service;

import com.cric.score.domain.Score;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;

@Service
@Scope("singleton")
public class ScoreService {
   private Score score;

    public Score getScore() {
        return score;
    }

    public boolean setScore(Score score) {
        this.score = score;
        return true;
    }
}
